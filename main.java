import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
            Scanner sc=new Scanner(System.in);
            System.out.println("================================");
            String number = "";
            int n = 0;
            String padding = "";
            for(int i=0;i<3;i++){
                String s1=sc.next();
                int x=sc.nextInt();
                //Complete this line
                n = 15-s1.length();
                padding = "";
                for (int j=0; j<n; j++){
                    padding = padding + " ";
                }
                number = Integer.toString(x);
                if (number.length() == 1){
                    number = "00" + number;
                }
                if (number.length() == 2){
                    number = "0" + number;
                }
                System.out.println(s1+padding+number);
            }
            System.out.println("================================");

    }
}

